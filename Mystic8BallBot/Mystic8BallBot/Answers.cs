﻿namespace Mystic8BallBot
{
    public class Answers
    {
        // Positive answers

        public const string ItIsCertain  = "It is certain";
        public const string ItIsDecidedlySo = "It is decidedly so";
        public const string WithoutADoubt = "Without a doubt";
        public const string YesDefinitely  = "Yes — definitely";
        public const string YouMayRelyOnIt = "You may rely on it";

        // Hesitantly positive

        public const string AsISeeItYes  = "As I see it, yes";
        public const string MostLikely  = "Most likely";
        public const string OutlookGood  = "Outlook good";
        public const string SignsPointToYes  = "Signs point to yes";
        public const string Yes = "Yes";

        // Neutral

        public const string ReplyHazyTryAgain  = "Reply hazy, try again";
        public const string AskAgainLater = "Ask again later";
        public const string BetterNotTellYouNow = "Better not tell you now";
        public const string CannotPredictNow = "Cannot predict now";
        public const string ConcentrateAndAskAgain  = "Concentrate and ask again";

        // Negative

        public const string DontCountOnIt = "Don’t count on it";
        public const string MyReplyIsNo  = "My reply is no";
        public const string MySourcesSayNo  = "My sources say no";
        public const string OutlookNotSoGood  = "Outlook not so good";
        public const string VeryDoubtful  = "Very doubtful";
    }
}