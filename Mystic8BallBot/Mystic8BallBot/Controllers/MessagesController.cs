﻿using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Bot.Connector;
using Microsoft.Bot.Connector.Utilities;

namespace Mystic8BallBot.Controllers
{
    [BotAuthentication]
    public class MessagesController : ApiController
    {
        private readonly Predictor predictor;

        public MessagesController()
        {
            this.predictor = new Predictor();
        }

        /// <summary>
        /// POST: api/Messages
        /// Receive a message from a user and reply to it
        /// </summary>
        public async Task<Message> Post([FromBody]Message message)
        {
            if (message.Type != "Message")
            {
                return null;
            }

            var prediction = this.predictor.GetPrediction();
            return message.CreateReplyMessage(prediction);
        }
    }
}