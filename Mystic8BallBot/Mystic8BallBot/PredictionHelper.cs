﻿namespace Mystic8BallBot
{
    public class PredictionHelper
    {
        public static string GetAnswer(int answerNumber)
        {
            switch (answerNumber)
            {
                case 0:
                    return Answers.ItIsCertain;
                case 1:
                    return Answers.ItIsDecidedlySo;
                case 2:
                    return Answers.WithoutADoubt;
                case 3:
                    return Answers.YesDefinitely;
                case 4:
                    return Answers.YouMayRelyOnIt;
                case 5:
                    return Answers.AsISeeItYes;
                case 6:
                    return Answers.MostLikely;
                case 7:
                    return Answers.OutlookGood;
                case 8:
                    return Answers.SignsPointToYes;
                case 9:
                    return Answers.Yes;
                case 10:
                    return Answers.ReplyHazyTryAgain;
                case 11:
                    return Answers.AskAgainLater;
                case 12:
                    return Answers.BetterNotTellYouNow;
                case 13:
                    return Answers.CannotPredictNow;
                case 14:
                    return Answers.ConcentrateAndAskAgain;
                case 15:
                    return Answers.DontCountOnIt;
                case 16:
                    return Answers.MyReplyIsNo;
                case 17:
                    return Answers.MySourcesSayNo;
                case 18:
                    return Answers.OutlookNotSoGood;
                case 19:
                    return Answers.VeryDoubtful;
                default:
                    return string.Empty;
            }
        }
    }
}