﻿using System;

namespace Mystic8BallBot
{
    public class Predictor
    {
        private const int MinAnswerNumber = 0;
        private const int MaxAnswerNumber = 19;

        private readonly Random random;

        public Predictor()
        {
            this.random = new Random(DateTime.UtcNow.Millisecond);
        }

        public string GetPrediction()
        {
            var answerNumber = random.Next(MinAnswerNumber, MaxAnswerNumber);
            return PredictionHelper.GetAnswer(answerNumber);
        }
    }
}