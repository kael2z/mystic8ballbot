﻿using System.Threading.Tasks;
using Microsoft.Bot.Connector;
using Mystic8BallBot.Controllers;
using NUnit.Framework;

namespace Mystic8BallBotTests.Controllers
{
    [TestFixture]
    public class MessagesControllerTests
    {
        [Test]
        public async Task PostMethodReturnsNotEmptyMessage()
        {
            // Prepare
            var message = new Message() { Type = "Message", Text = "Will I have a billion?" };
            var messageController = new MessagesController();

            // Execute
            var resultMessage = await messageController.Post(message);

            // Validate
            Assert.IsNotEmpty(resultMessage.Text);
        }

        [Test]
        public async Task PostMethodReturnsNullIfMessageDoesNotHaveTypeMessage()
        {
            // Prepare
            var message = new Message() { Type = "Ping", Text = "Will I have a billion?" };
            var messageController = new MessagesController();

            // Execute
            var resultMessage = await messageController.Post(message);

            // Validate
            Assert.IsNull(resultMessage);
        }
    }
}
