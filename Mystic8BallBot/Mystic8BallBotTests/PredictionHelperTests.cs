﻿using System.Collections.Generic;
using System.Linq;
using Mystic8BallBot;
using NUnit.Framework;

namespace Mystic8BallBotTests
{
    [TestFixture]
    public class PredictionHelperTests
    {
        [Test]
        public void GetAnswerReturnsEmptyStringIfAnwerNumberIsOutOfRange()
        {
            // Execute
            var negativeOutOfRangeAnswerNumberResult = PredictionHelper.GetAnswer(-1);
            var positiveOutOfRangeAnswerNumberResult = PredictionHelper.GetAnswer(20);

            // Validate
            Assert.AreEqual(string.Empty, negativeOutOfRangeAnswerNumberResult);
            Assert.AreEqual(string.Empty, positiveOutOfRangeAnswerNumberResult);
        }

        [Test]
        public void GetAnswerReturnsNotEmptyStringIfAnwerNumberIsInRange()
        {
            for (var i = 0; i < 20; i++)
            {
                // Execute
                var result = PredictionHelper.GetAnswer(i);

                // Validate
                Assert.AreNotEqual(string.Empty, result, "Answer for {0} number is missed.", i);
            }
        }

        [Test]
        public void GetAnswerReturnsDifferentAnswersForDifferentAnswerNumbers()
        {
            var answerList = new List<string>();
            for (var i = 0; i < 20; i++)
            {
                // Execute
                var result = PredictionHelper.GetAnswer(i);

                // Validate
                Assert.IsFalse(answerList.Any(a => a.Equals(result)), "Answer for {0} number is duplicate.", i);

                answerList.Add(result);
            }
        }
    }
}
