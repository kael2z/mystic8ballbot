# The Mystic 8 Ball Bot #

The Mystic 8 Ball bot has answers for any questions. Fill free to ask him yes-no question.

The bot default page: http://mystic8ballbot.azurewebsites.net/

### You can talk to the bot in the messaging apps: ###
* Telegram: http://telegram.me/mystic8ballbot
* Skype: https://join.skype.com/bot/167e24a9-695c-4e08-b542-d3c398c5e515

### What is this repository for? ###

* This is a code example of simple stateless bot.
* Fill free to use the code for learning purposes.

### How can I create and deploy a bot? ###

* Get started article about how to create and deploy your bot:
https://habrahabr.ru/company/microsoft/blog/281459/

### Useful links ###

* Deploy your app to Azure App Service:
https://azure.microsoft.com/en-us/documentation/articles/web-sites-deploy/#vso

* Publishing to Azure Web Sites from any git/hg repo:
http://blog.davidebbo.com/2013/04/publishing-to-azure-web-sites-from-any.html

* Azure App Service plans in-depth overview:
https://azure.microsoft.com/en-us/documentation/articles/azure-web-sites-web-hosting-plans-in-depth-overview/